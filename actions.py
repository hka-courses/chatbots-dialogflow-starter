from google.cloud.dialogflow_v2 import DetectIntentResponse
from google.protobuf.json_format import MessageToDict

# This is for the actions

async def dummy_method(rsp: DetectIntentResponse) -> dict:
    res = MessageToDict(rsp._pb) # this is safer to overcome some issues with protobuf
    # do here your stuff and possibly modify the response
    return res

