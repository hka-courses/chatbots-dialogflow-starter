import json
from typing import Sequence, Callable, Awaitable
from uuid import uuid1

from google.auth.api_key import Credentials
from google.cloud.dialogflow_v2 import SessionsAsyncClient, DetectIntentResponse, QueryInput, DetectIntentRequest
from google.oauth2 import service_account
from google.protobuf.json_format import ParseDict, MessageToDict


def dialogflow_credentials() -> Credentials:
    """ Returns the project specific google cloud credentials """
    dialogflow_key = json.load(open(f"dialogflow_account.json"))
    return service_account.Credentials.from_service_account_info(dialogflow_key)


async def dialogflow_query(actions: dict[str, Callable[[DetectIntentResponse], Awaitable[dict]]],
                           detect_request: dict | None = None,
                           text: str | None = None, user: str | None = None, lang: str | None = None,
                           platform_filter: str | None = "ACTIONS_ON_GOOGLE",
                           message_type_filter: Sequence[str] | None = None) -> dict:
    """

    :param actions: mapping of actions (as in the intent definition) to methods
    :param detect_request: DetectIntentRequest (directly for DialogFlow)
    :param text: Alternative: only the text to be interpreted (needs lang)
    :param lang: language code
    :param user: Optional user for session generation
    :param platform_filter: Optional platform filter that returns only messages for a particular platform
    :param message_type_filter: Optional message filter that returns only messages of a particular type
    :return: DetectIntentResponse
    """

    credentials = dialogflow_credentials()
    project_id = credentials.project_id
    dialogflow = SessionsAsyncClient(credentials=credentials)

    # textual query is given
    if text is not None:
        session = dialogflow.session_path(project_id, uuid1().hex if user is not None else user.replace('@', ''))
        query_input = QueryInput(
            text=TextInput(text=text, language_code=lang))  # type: ignore
        response_dialogflow: DetectIntentResponse = await dialogflow.detect_intent(session=session,
                                                                                   query_input=query_input)
    else:
        # DetectIntentRequest is given

        # Generate a proper session id if not conforming to spec
        session = detect_request.get("session")
        if session is None:
            session = dialogflow.session_path(project_id, uuid1().hex)
        elif not session.startswith("project"):
            session = dialogflow.session_path(project_id, session)
        detect_request["session"] = session

        response_dialogflow: DetectIntentResponse = await dialogflow.detect_intent(
            request=ParseDict(detect_request, DetectIntentRequest()._pb))

    # Process actions
    if (a := response_dialogflow.query_result.action) is not None and a in actions:
        return await actions[a](response_dialogflow)

    return filter_messages(MessageToDict(response_dialogflow._pb), platform_filter, message_type_filter)


def filter_messages(data: dict, platform: str | None, message_type_filter: Sequence[str] | None = None) -> dict:
    """ Filters the messages from DialogFlow according to message type or platform """

    if platform is not None:
        ms = [m for m in data["queryResult"]["fulfillmentMessages"] if
              "platform" in m and m["platform"] == platform]
        if len(ms) > 0:
            data["queryResult"]["fulfillmentMessages"] = ms

    if message_type_filter is not None:
        ms = [m for m in data["queryResult"]["fulfillmentMessages"] if m["type"] in message_type_filter]
        data["queryResult"]["fulfillmentMessages"] = ms

    return data
