import json

import uvicorn
from fastapi import FastAPI, WebSocket, Request
from google.cloud.dialogflow_v2 import WebhookRequest
from google.protobuf.json_format import ParseDict

from actions import dummy_method
from dialogflow import dialogflow_query, dialogflow_credentials

ACTIONS = {
    # place action names and method to handle it (should take a DetectIntentResponse as a paramater)
    "action_name": dummy_method
}

app = FastAPI()


@app.get("/")
async def gateway_bot():
    """ Returns the agent description needed for the DialogFlow Web V2 UI
        https://github.com/mishushakov/dialogflow-web-v2

        Only needed if this UI is used.
    """
    credentials = dialogflow_credentials()
    project_id = credentials.project_id

    return {"displayName": "<The bot's name>",
            "parent": f"projects/{project_id}",
            "description": """
                The description to be displayed.
                """,
            "defaultLanguageCode": "en",
            "supportedLanguageCodes": ["en"],
            "timeZone": "Europe/Berlin",
            "enableLogging": True,
            "matchMode": "MATCH_MODE_HYBRID",
            "classificationThreshold": 0.3,
            "avatarUri": "https://materials.hka-cloud.de/resources/icons/bot/chatbot_profilbild-300x300.png",
            }


@app.websocket("/")
async def detect_intent_ws(websocket: WebSocket):
    """ Accepts DetectIntentRequests via WebSockets connection.
        Forwards the DetectIntentRequest to DialogFlow

        https://github.com/mishushakov/dialogflow-web-v2

        Only needed if this UI is used.
    """
    await websocket.accept()
    while True:
        data = await websocket.receive_text()
        res = await dialogflow_query(detect_request=json.loads(data), actions=ACTIONS)
        await websocket.send_text(json.dumps(res))


@app.post('/')
async def detect_intent(request: Request) -> dict:
    """ Method for dialogflow gateway """
    return await dialogflow_query(detect_request=await request.json(), actions=ACTIONS)


@app.get('/query')
async def direct_query(text: str, user: str | None = None):
    """ Directly access dialogflow using the text as query input.
        This can be used for testing
        Optional user is only for session generation.
    """
    return await dialogflow_query(text=text, user=user, actions=ACTIONS)

@app.post('/webhook')
async def webhook(request: Request):
    webhook_request = await request.json()
    r = ParseDict(webhook_request, WebhookRequest()._pb)



if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5000)
