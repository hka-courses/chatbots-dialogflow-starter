from typing import Sequence


def create_button(title: str, url: str) -> dict:
    return {
        "title": title,
        "openUriAction": {
            "uri": url
        }
    }


def basic_card(title: str, description: str, action: str | None = None, url: str | None = None,
               image_uri: str | None = None,
               image_text: str | None = None, buttons: list[dict] | None = None) -> dict:
    return {
        "platform": "ACTIONS_ON_GOOGLE",
        "basicCard": {
            "title": title,
            "formattedText": description,
            "image": {} if image_uri is None else {"imageUri": image_uri,
                                                   "accessibilityText": image_text if image_text is not None else description},
            "buttons": buttons if buttons is not None else [create_button(action, url)]
        }
    }


def card(title: str, subtitle: str, image_uri: str, action: str | None, postback: str | None,
         buttons: dict | None = None) -> dict:
    return {
        "card": {
            "title": title,
            "subtitle": subtitle,
            "imageUri": image_uri,
            "buttons": buttons if buttons is not None else [create_plain_button(action, postback)]
        }
    }


def create_plain_button(action: str, postback: str) -> dict:
    return {
        "text": action,
        "postback": postback
    }


def simple_response(txt: str) -> dict:
    return {
        "platform": "ACTIONS_ON_GOOGLE",
        "simpleResponses": {
            "simpleResponses": [
                {
                    "textToSpeech": txt
                }
            ]
        }
    }


def linkout_suggestion(title: str, url: str) -> dict:
    return {
        "linkOutSuggestion":
            {
                "destinationName": title,
                "uri": url
            },
        "platform": "ACTIONS_ON_GOOGLE"
    }


def list_select(items: list[dict], title: str):
    return {
        "listSelect": {"items": items, "title": title},
        "platform": "ACTIONS_ON_GOOGLE"
    }


def create_item(title: str, description: str, key: str, image_object: dict | None = None, synonyms=None) -> dict:
    return {
        "image": image_object if image_object is not None else {},
        "description": description,
        "info": {"key": key, "synonyms": synonyms if synonyms is not None else []},
        "title": title
    }


def suggestion_chips(suggestions: Sequence[str]) -> dict:
    return {
        "suggestions": {
            "suggestions": [{"title": s} for s in suggestions]
        }
    }


def quick_replies(suggestions=str | Sequence[str]) -> dict:
    return {
        "quickReplies": {
            [suggestions] if isinstance(suggestions, str) else suggestions
        }
    }


def image(uri: str, description: str) -> dict:
    return {
        "image": {
            "imageUri": uri,
            "accessibilityText": description
        }
    }


def text(txt: str | Sequence[str]) -> dict:
    return {
        "text": [txt] if isinstance(text, str) else text
    }


def media(objects: Sequence[dict] | dict, media_type: str = "AUDIO") -> dict:
    return {
        "mediaType": media_type,
        "mediaObjects": [objects] if isinstance(objects, dict) else objects
    }


def create_media_object(name: str, url: str, description: str | None = None, large_image: str | None = None,
                        icon: str | None = None) -> dict:
    r = {
        "name": name,
        "description": "" if description is None else description,
        "contentUrl": url
    }

    if large_image is not None:
        r["largeImage"] = {
            "imageUri": large_image
        }
    elif icon is not None:
        r["icon"] = {
            "imageUri": large_image
        }

    return r
